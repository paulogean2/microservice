from django.http import JsonResponse

# Create your views here.
def teste(request, numero):
    response = opa(numero)
    return JsonResponse(response)

def opa(numero: int):
    if numero > 0:
        return {"hey": f'{numero}'}
    elif numero < 0:
        return {"hey": 'negativo'}
    else:
        return {"ho": "let's go"}
